---
title: Graphs
---

## Slides

{% include slides.html id="graphs" %}

## Videos

### Part 1

{% include video.html id="0_euft3az9" %}

### Part 2

{% include video.html id="0_ngljixmt" %}

### Part 3

{% include video.html id="0_554d8ol7" %}

