---
title: Balanced Trees
---

## Slides

{% include slides.html id="balanced-trees" %}

## Videos

### Part 1

{% include video.html id="0_7gan25ob" %}

### Part 2

{% include video.html id="0_nthf58ch" %}

### Part 3

{% include video.html id="0_vo0wcij6" %}

### Part 4

{% include video.html id="0_7dx8879b" %}

### Part 5

{% include video.html id="0_13o1rsku" %}
