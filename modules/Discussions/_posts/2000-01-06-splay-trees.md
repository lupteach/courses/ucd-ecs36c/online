---
title: Splay Trees
---

## Worksheet

{% include worksheet.html id="splay-trees" %}

## Video

{% include video.html id="0_72vwmmyv" %}

*Note that the discussion shows a top-down splaying approach for insertions; the
new key is first inserted, and then the tree splayed starting from the root
node. There is also a bottom-up approach, where the tree is splayed during the
return from recursion (by adding a call to `Spray(n, key);` at the end of the
recursive `Insert()` function).*
