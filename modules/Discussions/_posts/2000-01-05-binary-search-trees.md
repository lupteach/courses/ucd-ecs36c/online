---
title: Binary Trees
---

## Worksheet

{% include worksheet.html id="binary-search-trees" %}

## Video

{% include video.html id="0_kf962whm" %}

*Unfortunately, the CheckBST() function in Q2.3 is not 100% correct, as it misses some use cases. Here is an updated, and commented, version that fully works:*

```cpp
template <typename K>
bool BST<K>::CheckBST() {
  // One pointer to keep track of the predecessor
  // Initialized to null because the first node that will get analyzed is the
  // smallest one and doesn't have a predecessor
  Node *pred = nullptr;
  return CheckBST(root.get(), pred);
}

template <typename K>
bool BST<K>::CheckBST(Node *n, Node *&pred) {
  if (!n)
    // Empty tree is a BST
    return true;

  // Recurse to the left
  if (!CheckBST(n->left.get(), pred))
    return false;

  // Analyze current node, and compare to predecessor
  if (pred && n->key <= pred->key)
    return false;
  // Now current node becomes the predecessor
  // (either to its right child, or to one of its ancestors)
  pred = n;

  // Recurse on the right
  return CheckBST(n->right.get(), pred);
}
```
