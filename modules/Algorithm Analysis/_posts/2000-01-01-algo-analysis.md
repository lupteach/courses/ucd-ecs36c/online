---
title: Algorithm Analysis
---

## Slides

{% include slides.html id="algo-analysis-lect" %}

## Videos

### Part 1

{% include video.html id="0_e8xx79vy" %}

### Part 2

{% include video.html id="0_kd39n6ay" %}

### Part 3

{% include video.html id="0_mctf8gen" %}
