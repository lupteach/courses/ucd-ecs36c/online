---
title: Priority Queues
---

## Slides

{% include slides.html id="priority-queues" %}

## Videos

### Part 1

{% include video.html id="0_vshgsi3k" %}

### Part 2

{% include video.html id="0_ylcuati1" %}

