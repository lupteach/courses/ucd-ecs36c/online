---
layout: index
published: true
image: ./img/ecs36c.png
---

# Welcome!

Hi all, my name is [Joël
Porquet-Lupine](https://cs.ucdavis.edu/directory/joel-porquet-lupine). I am
currently an Assistant Professor of Teaching at UC Davis.

ECS 36C is a course that I really enjoy teaching as it represents such a crucial
experience in the students' learning experience. It is the course that ends the
introductory series and enables students to start taking most of the
upper-division courses.

I gave this course a few times now, and have probably spent a few hundreds of
hours working on my slides, especially for all the figures! And because of the
pandemic, I also had to record lecture videos in spring of 2020.

This means that all the "lecturing" part of this course is now available
digitally, which I am happy to release entirely via this website under a [CC
BY-NC-SA 4.0 license](http://creativecommons.org/licenses/by-nc-sa/4.0/).

I hope that anyone who is interested in learning about data structures and
algorithms will find this course material useful!

# Course outline

## Lectures

ECS 36C is a 10-week course, but when accounting for holidays and/or exams,
there are about 9 weeks worth of lecture time.

The lecture material is broken up into multiple parts:

- [Algorithm analysis](./modules/algorithm analysis/algo-analysis/): ~1 week
- Lists, Stacks and Queues: ~1.5 weeks
    * [Lists](./modules/lists, stacks and queues/lists/)
    * [Stacks and Queues](./modules/lists, stacks and queues/stacks-queues/)
- Trees: ~3 weeks
    * [Trees](./modules/trees/trees/)
    * [Balanced Trees](./modules/trees/balanced-trees/)
- [Priority Queues](./modules/priority queues/priority-queues/): ~0.75 weeks
- [Graphs](./modules/graphs/graphs/): ~1 week
- [Sorting](./modules/sorting/sorting/): ~0.75 weeks
- [Hashing](./modules/hashing/hashing/): ~1 weeks

## Discussions

There is an hourly discussion per week, which is organized as a very hands-on
experience around the concepts discussed during lectures.

- Week #1: [C++ and UNIX review](./modules/discussions/cpp-unix/)
- Week #2: [Algorithm Analysis](./modules/discussions/algo-analysis/)
- Week #3: [Doubly Linked List](./modules/discussions/doubly-ll/)
- Week #4: [Stacks/Queues and Binary Trees](./modules/discussions/stacks-queues-binary-trees/)
- Week #5: [Binary Trees](./modules/discussions/binary-search-trees/)
- Week #6: [Splay Trees](./modules/discussions/splay-trees/)
- Week #7: [Tries](./modules/discussions/tries/)
- Week #8: [Topological Sort](./modules/discussions/topological-sort/)
- Week #9: [External Sorting](./modules/discussions/external-sorting/)
- Week #10: [Cuckoo Hashing](./modules/discussions/cuckoo-hashing/)
