# ECS 36C @ UC Davis

This repo contains the class materials created by Prof. Joël Porquet-Lupine for
the course *ECS 36C: Data Structures, Algorithms, and Programming*.

It contains all of the lecture and discussion materials.

The videos were created during spring quarter 2020.

All of this work is shared under a [CC BY-NC-SA 4.0 International
License](https://creativecommons.org/licenses/by-nc-sa/4.0/).

Copyright © 2018-2021 Joël Porquet-Lupine.
